import React from "react";
import styles from "./Formulario.module.css";
import useSelect from "../hooks/useSelect";
import PropTypes from "prop-types";

const Formulario = ({ setCategoria }) => {
  const OPCIONES = [
    { value: "general", label: "General", cod: 1 },
    { value: "business", label: "Negocios", cod: 2 },
    { value: "entertainment", label: "Entretenimiento", cod: 3 },
    { value: "health", label: "Salud", cod: 4 },
    { value: "science", label: "Ciencia", cod: 5 },
    { value: "technology", label: "Tecnologia", cod: 6 },
    { value: "sports", label: "Deportes", cod: 7 },
  ];

  // Custom hook
  const [categoria, SelectNoticias] = useSelect("general", OPCIONES);

  // Pasar la categoria al App.js
  const buscarNoticias = (e) => {
    e.preventDefault();
    setCategoria(categoria);
  };

  return (
    <div className={`${styles.buscador} row`}>
      <div className="col s12 m8 offset-m2">
        <form onSubmit={buscarNoticias}>
          <h3 className={styles.heading}>Encuentra noticias por categoria</h3>
          <SelectNoticias />
          <div className="input-field col s12">
            <input
              type="submit"
              className={`${styles.btn_block} btn-large amber darken-2`}
              value="Buscar"
            />
          </div>
        </form>
      </div>
    </div>
  );
};

Formulario.propTypes = {
  setCategoria: PropTypes.func.isRequired,
};

export default Formulario;
